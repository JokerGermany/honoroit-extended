module gitlab.com/etke.cc/honoroit

go 1.17

// replace gitlab.com/etke.cc/linkpearl => ../linkpearl

require (
	git.sr.ht/~xn/cache v1.2.2
	github.com/getsentry/sentry-go v0.12.0
	github.com/lib/pq v1.10.4
	github.com/mattn/go-sqlite3 v1.14.12
	github.com/stretchr/testify v1.7.1
	gitlab.com/etke.cc/linkpearl v0.0.0-20220318212535-8b9bbca94845
	maunium.net/go/mautrix v0.10.12
)

require (
	github.com/bradfitz/gomemcache v0.0.0-20220106215444-fb4bf637b56d // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/tidwall/gjson v1.14.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/tidwall/sjson v1.2.4 // indirect
	golang.org/x/crypto v0.0.0-20220315160706-3147a52a75dd // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
	maunium.net/go/maulogger/v2 v2.3.2 // indirect
)
