package config

// Config of Honoroit
type Config struct {
	// Homeserver url
	Homeserver string
	// Login is a MXID localpart (honoroit - OK, @honoroit:example.com - wrong)
	Login string
	// Password for login/password auth only
	Password string
	// RoomID where threads will be created
	RoomID string
	// Prefix for honoroit commands
	Prefix string
	// LogLevel for logger
	LogLevel string
	// CacheSize max amount of items in cache
	CacheSize int

	// Text messages
	Text Text

	// DB config
	DB DB

	// Sentry DSN
	Sentry string
}

// DB config
type DB struct {
	// DSN is a database connection string
	DSN string
	// Dialect of the db, allowed values: postgres, sqlite3
	Dialect string
}

// Text messages
type Text struct {
	// PrefixOpen is a prefix added to new thread topics
	PrefixOpen string
	// PrefixDone is a prefix added to threads marked as done/closed
	PrefixDone string

	// Greetings message sent to customer on first contact
	Greetings string
	// Error message sent to customer if something goes wrong
	Error string
	// EmptyRoom message sent to backoffice/threads room when customer left his room
	EmptyRoom string
	// Start message that sent into the read as result of the "start" command
	Start string
	// Done message sent to customer when request marked as done in the threads room
	Done string
}
